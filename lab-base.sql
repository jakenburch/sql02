/*
 Navicat MySQL Data Transfer

 Source Server         : cgt356-vagrant
 Source Server Type    : MySQL
 Source Server Version : 50544
 Source Host           : 192.168.3.56
 Source Database       : lab-db

 Target Server Type    : MySQL
 Target Server Version : 50544
 File Encoding         : utf-8

 Date: 10/16/2015 09:17:36 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `pizza_topping`
-- ----------------------------
DROP TABLE IF EXISTS `pizza_topping`;
CREATE TABLE `pizza_topping` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `pizza_topping`
-- ----------------------------
BEGIN;
INSERT INTO `pizza_topping` VALUES ('topping_1', 'peperoni'), ('topping_2', 'anchovies'), ('topping_3', 'jelly beans'), ('topping_4', 'peanut butter'), ('topping_5', 'chocolate chip');
COMMIT;

-- ----------------------------
--  Table structure for `thing`
-- ----------------------------
DROP TABLE IF EXISTS `thing`;
CREATE TABLE `thing` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `thing`
-- ----------------------------
BEGIN;
INSERT INTO `thing` VALUES ('thing_1', 'cheese'), ('thing_10', 'mutations'), ('thing_2', 'bo staff'), ('thing_3', 'yellow jumpsuit'), ('thing_4', 'learning'), ('thing_5', 'katana'), ('thing_6', 'sai'), ('thing_7', 'nunchucks'), ('thing_8', 'wisdom'), ('thing_9', 'facemask');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pizza_topping_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('user_1', 'Leo', 'topping_4'), ('user_2', 'Don', 'topping_5'), ('user_3', 'Raph', 'topping_5'), ('user_4', 'Mic', 'topping_3'), ('user_5', 'Splinter', 'topping_1');
COMMIT;

-- ----------------------------
--  Table structure for `user_favorite_thing`
-- ----------------------------
DROP TABLE IF EXISTS `user_favorite_thing`;
CREATE TABLE `user_favorite_thing` (
  `user_id` varchar(255) NOT NULL,
  `thing_id` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`,`thing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `user_favorite_thing`
-- ----------------------------
BEGIN;
INSERT INTO `user_favorite_thing` VALUES ('user_1', 'thing_10'), ('user_1', 'thing_3'), ('user_1', 'thing_4'), ('user_1', 'thing_5'), ('user_2', 'thing_2'), ('user_3', 'thing_6'), ('user_4', 'thing_7'), ('user_5', 'thing_4'), ('user_5', 'thing_8');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
